class Video:

    def __init__ (self, path):
        self.path = path


    from enum import Enum
    class Output(Enum):
        FILE_DIR = 1
        PROJECT_DIR = 2


    def SplitOnFrames (self, name_output_file, quality, directory, ):
        import cv2 as cv
        import os.path
        from pathlib import Path
        from colorama import Fore
        from progress.bar import IncrementalBar

        # DIVIDE PATH ON DIRECTORY AND FILE NAME
        path_file_dir, file_name = os.path.split(str(self.path))

        # CREATE FILE IN DIRECTION
        if directory == Video.Output.FILE_DIR:
            output_path = Path(path_file_dir)
            output_path = output_path.joinpath(output_path, r'' + name_output_file)
            if not output_path.exists():
                output_path.mkdir()
            os.chdir(output_path)
        if directory == Video.Output.PROJECT_DIR:
            current_directory = os.getcwd()
            output_path = os.path.join(current_directory, r'' + name_output_file)
            if not os.path.exists(output_path):
                os.makedirs(output_path)
            os.chdir(output_path)

        # EXTRACT FRAMES
        print(Fore.BLUE + f'Extracting frames to:   {output_path}' + Fore.RESET)
        cap = cv.VideoCapture(self.path, 0)
        count = 0
        success = True
        bar = IncrementalBar('Processing', max=cap.get(cv.CAP_PROP_FRAME_COUNT))
        while success:
            success, image = cap.read()
            cv.imwrite("%06d.jpg" % count, image, [cv.IMWRITE_JPEG_QUALITY, quality])
            # print('FRAME:',"%06d.jpg" % count,'   Compute:', success)
            count += 1
            bar.next()
        bar.finish()
        print(Fore.GREEN + 'DONE FRAMES EXTRACT' + Fore.RESET)

    '''
from enum import Enum
class Output(Enum):
    FILE_DIR = 1
    PROJECT_DIR = 2


def SplitOnFrames(self,input_path, name_input_file, name_output_file, quality, directory, ):
        import cv2 as cv
        import os.path
        from pathlib import Path
        from colorama import Fore
        from progress.bar import IncrementalBar

        # CREATE FILE IN DIRECTION
        if directory== Output.FILE_DIR:
            output_path = Path(input_path)
            output_path = output_path.joinpath(output_path, r'' + name_output_file)
            if not output_path.exists():
                output_path.mkdir()
            os.chdir(output_path)
        if directory== Output.PROJECT_DIR:
            current_directory = os.getcwd()
            output_path = os.path.join(current_directory, r'' + name_output_file)
            if not os.path.exists(output_path):
                os.makedirs(output_path)
            os.chdir(output_path)

        # EXTRACT FRAMES
        cap = cv.VideoCapture(input_path + name_input_file, 0)
        count = 0
        success = True
        bar = IncrementalBar('Processing', max=cap.get(cv.CAP_PROP_FRAME_COUNT))
        while success:
            success,image = cap.read()
            # cv.imwrite("%06d.jpg" % count, image, [cv.IMWRITE_JPEG_QUALITY, quality])
            # print('FRAME:',"%06d.jpg" % count,'   Compute:', success)
            count += 1
            bar.next()
        bar.finish()
        print(Fore.GREEN+'DONE'+Fore.RESET)
        print(Fore.BLUE+f'Files extract to:   {output_path}'+Fore.RESET)
        
        
        
SplitOnFrames( input_path='/Users/lukaszpluzynski/Desktop/folder bez nazwy/',
               name_input_file='Movie_02.mov
               name_output_file='Movie_02_FRAMES_4',
               quality=100,
               directory=Output.FILE_DIR)
'''
